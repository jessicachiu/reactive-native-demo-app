import * as React from 'react';
import { View, Text } from 'react-native';
import { FlatList } from 'react-native';

interface User {
    id: number;
    name: string
}

const data: User[] = [
    {id: 1, name: 'Alex'},
    {id: 2, name: 'Gordon'},
    {id: 3, name: 'Micheal'},
    {id: 4, name: 'JAson'},
]

export default function About(){
    return (
        <View>
            <Text>About</Text>
            <FlatList<User>
                data={data}
                // renderItem={row => (
                //     <View style={{height: 200, padding: 10, borderBottomColor: '#cccccc', borderBottomWidth: 5}}>
                //         <Text>{row.item.name}</Text>
                //     </View>
                // )}
                renderItem={row => {
                    console.log("rendering" + row.item.id)
                    return(
                        <View style={{height: 200, padding: 10, borderBottomColor: '#cccccc', borderBottomWidth: 5}}>
                            <Text>{row.item.name}</Text>
                        </View>
                    )
                }}
            />
        </View>
    )
}
