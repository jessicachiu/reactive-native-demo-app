import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


import Home from './Home';
import About from './About';
import Setting from './Setting';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const SettingStack = createNativeStackNavigator();

const SettingPage = () =>{
    return (
        <SettingStack.Navigator>
            <SettingStack.Screen name="Setting" component={Setting}/>
        </SettingStack.Navigator>
    )
}

const MainPage = () =>{
    return (
        <Tab.Navigator>
            <Tab.Screen name="Home" component={Home} options={{headerShown: false}}/>
            <Tab.Screen name="tabAbout" component={About} options={{headerShown: false}} />
            <Tab.Screen name="Setting" component={SettingPage} options={{headerShown: false}} />
        </Tab.Navigator>
    )
}




export default function App(){
    return (
        <NavigationContainer>
             <Stack.Navigator>
                <Stack.Screen name="Main" component={MainPage} options={{
                    headerShown: false
                }} />
                <Stack.Screen name="About" component={About} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}